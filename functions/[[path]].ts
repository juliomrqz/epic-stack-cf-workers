import { app } from "#server/index.js";
import { handle } from 'hono/cloudflare-pages';

export const onRequest: PagesFunction = handle(app)