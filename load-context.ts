import { init } from "./app/utils/env.server.js";
import { type AppLoadContext } from "@remix-run/cloudflare";
import { type PlatformProxy } from "wrangler";

// When using `wrangler.toml` to configure bindings,
// `wrangler types` will generate types for those bindings
// into the global `Env` interface.
// Need this empty interface so that typechecking passes
// even if no `wrangler.toml` exists.
// eslint-disable-next-line @typescript-eslint/no-empty-interface
// interface Env {}

type Cloudflare = Omit<PlatformProxy<Env>, "dispose">;

declare module "@remix-run/cloudflare" {
  interface AppLoadContext {
    cloudflare: Cloudflare;
    // extra: string; // augmented
    cspNonce?: string;
    ENV: Env
  }
}

type GetLoadContext = (args: {
    request: Request;
    context: { cloudflare: Cloudflare }; // load context _before_ augmentation
  }) => AppLoadContext;
  
  // Shared implementation compatible with Vite, Wrangler, and Cloudflare Pages
  export const getLoadContext: GetLoadContext = ({
    context,
    request,
    ...extra
  }) => {
    console.log('context', context)
    // console.log('request', request.)

    return {
      ...context,
    //   extra: "stuff",
      cspNonce: undefined, // TODO
      // ENV: init(context.cloudflare.env),
      ENV: context.cloudflare.env,
    };
  };