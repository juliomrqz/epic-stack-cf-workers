import { createPagesFunctionHandler as _createRequestHandler } from "@remix-run/cloudflare-pages";
import type { ServerBuild } from '@remix-run/cloudflare'
// import * as Sentry from '@sentry/remix'
// import compression from 'compression'
// import express from 'express'
// import rateLimit from 'express-rate-limit'
import {  Hono } from 'hono'
import { trimTrailingSlash } from 'hono/trailing-slash'
import { secureHeaders } from 'hono/secure-headers'
import { logger } from 'hono/logger'
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore - the server build file is generated by `remix vite:build`
// eslint-disable-next-line import/no-unresolved
import * as build from "../build/server";
import { getLoadContext } from "../load-context";
import type { Bindings, Variables } from "./types";

const MODE = process.env.NODE_ENV ?? 'development'
const IS_PROD = MODE === 'production'
// const IS_DEV = MODE === 'development'

// const createRequestHandler = IS_PROD
// 	? Sentry.wrapExpressCreateRequestHandler(_createRequestHandler)
// 	: _createRequestHandler
const createRequestHandler = _createRequestHandler({
	build: build as unknown as ServerBuild,
	getLoadContext,
	mode: MODE,
});

  
const viteDevServer = IS_PROD
	? undefined
	: await import('vite').then(vite =>
			vite.createServer({
				server: { middlewareMode: true },
			}),
		)

// crypto.randomBytes(16).toString('hex')
const generateRandomString = (length = 64) => {
	const array = new Uint8Array(10);
	crypto.getRandomValues(array);
	return Array.from(array)
		.map((byte) => byte.toString(16).padStart(2, '0'))
		.join('');
	};

export const app = new Hono<{ Bindings: Bindings, Variables: Variables }>()

// const getHost = (c: Context) =>
// 	c.req.header('X-Forwarded-Host') ?? c.req.header('host') ?? ''

// fly is our proxy
// app.set('trust proxy', true)

// ensure HTTPS only (X-Forwarded-Proto comes from Fly)
// app.use((c, next) => {
// 	const proto = c.req.header('X-Forwarded-Proto')
// 	const host = getHost(c)
// 	if (proto === 'http') {
// 		c.header('X-Forwarded-Proto', 'https')
// 		c.redirect(`https://${host}${c.req.originalUrl}`)
// 		return
// 	}
// 	next()
// })

// no ending slashes for SEO reasons
// https://github.com/epicweb-dev/epic-stack/discussions/108
app.use(trimTrailingSlash())

// On Cloudflare Workers, the response body will be compressed automatically, so there is no need to use this middleware.
// app.use(compress())

// app.use(Sentry.Handlers.requestHandler())
// app.use(Sentry.Handlers.tracingHandler())

if (viteDevServer) {
	// app.use(viteDevServer.middlewares)
} else {
	// // Remix fingerprints its assets so we can cache forever.
	// app.use(
	// 	'/assets',
	// 	express.static('build/client/assets', { immutable: true, maxAge: '1y' }),
	// )

	// // Everything else (like favicon.ico) is cached for an hour. You may want to be
	// // more aggressive with this caching.
	// app.use(express.static('build/client', { maxAge: '1h' }))
}

// if we made it past the express.static for these, then we're missing something.
// So we'll just send a 404 and won't bother calling other middleware.
app.get('/img/*', c => c.notFound());
app.get('/favicons/*', c => c.notFound());

app.use(logger())


app.use(async (c, next) => {
	c.set('cspNonce',generateRandomString(16))

	await next()
})

app.use((c, next) => {
	const cspNonce = c.get('cspNonce');

	return secureHeaders({
		referrerPolicy: 'same-origin',
		crossOriginEmbedderPolicy: false,
		contentSecurityPolicy: {
			// NOTE: Remove reportOnly when you're ready to enforce this CSP
			// reportOnly: true,
			// directives: {
				connectSrc: [
					MODE === 'development' ? 'ws:' : null,
					process.env.SENTRY_DSN ? '*.sentry.io' : null,
					"'self'",
				].filter(Boolean),
				fontSrc: ["'self'"],
				frameSrc: ["'self'"],
				imgSrc: ["'self'", 'data:'],
				scriptSrc: [
					"'strict-dynamic'",
					"'self'",
					`'nonce-${cspNonce}'`,
				],
				styleSrcAttr: [
					`'nonce-${cspNonce}'`,
				],
				upgradeInsecureRequests: undefined,
			// },
		},
	})(c, next);
})

// When running tests or running in development, we want to effectively disable
// rate limiting because playwright tests are very fast and we don't want to
// have to wait for the rate limit to reset between tests.
// const maxMultiple =
// 	!IS_PROD || process.env.PLAYWRIGHT_TEST_BASE_URL ? 10_000 : 1
// const rateLimitDefault = {
// 	windowMs: 60 * 1000,
// 	max: 1000 * maxMultiple,
// 	standardHeaders: true,
// 	legacyHeaders: false,
// 	// Fly.io prevents spoofing of X-Forwarded-For
// 	// so no need to validate the trustProxy config
// 	validate: { trustProxy: false },
// }

// const strongestRateLimit = rateLimit({
// 	...rateLimitDefault,
// 	windowMs: 60 * 1000,
// 	max: 10 * maxMultiple,
// })

// const strongRateLimit = rateLimit({
// 	...rateLimitDefault,
// 	windowMs: 60 * 1000,
// 	max: 100 * maxMultiple,
// })

// const generalRateLimit = rateLimit(rateLimitDefault)
// app.use((req, res, next) => {
// 	const strongPaths = [
// 		'/login',
// 		'/signup',
// 		'/verify',
// 		'/admin',
// 		'/onboarding',
// 		'/reset-password',
// 		'/settings/profile',
// 		'/resources/login',
// 		'/resources/verify',
// 	]
// 	if (req.method !== 'GET' && req.method !== 'HEAD') {
// 		if (strongPaths.some(p => req.path.includes(p))) {
// 			return strongestRateLimit(req, res, next)
// 		}
// 		return strongRateLimit(req, res, next)
// 	}

// 	// the verify route is a special case because it's a GET route that
// 	// can have a token in the query string
// 	if (req.path.includes('/verify')) {
// 		return strongestRateLimit(req, res, next)
// 	}

// 	return generalRateLimit(req, res, next)
// })

// async function getBuild() {
// 	const build = viteDevServer
// 		? viteDevServer.ssrLoadModule('virtual:remix/server-build')
// 		: // @ts-ignore this should exist before running the server
// 			// but it may not exist just yet.
// 			await import('#build/server/index.js')
// 	// not sure how to make this happy 🤷‍♂️
// 	return build as unknown as ServerBuild
// }


app.use(async (c, next) => {
	// @ts-expect-error
	const allowIndexing = c.env.ALLOW_INDEXING === "true" || c.env.ALLOW_INDEXING === true

	if (!allowIndexing) {
		c.header('X-Robots-Tag', 'noindex, nofollow')
	}
	
	await next()
})


app.all('*', (c) => {
	// @ts-expect-error - TODO: verify type
	// return createRequestHandler({ ...c.event, env: {...c.env, cspNonce: c.get('cspNonce') || null} })
	return createRequestHandler(c.event)
})
