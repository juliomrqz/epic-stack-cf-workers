export type Bindings = Env & Record<string, unknown>
export type Variables = {cspNonce: string};
