import {
	type LoaderFunctionArgs,
	type ActionFunctionArgs,
	type HandleDocumentRequestFunction,
} from '@remix-run/cloudflare'
import { RemixServer } from '@remix-run/react'
// import * as Sentry from '@sentry/remix'
import { isbot } from 'isbot'
import { renderToReadableStream } from 'react-dom/server'
// import { getEnv, init } from './utils/env.server.ts'
// import { getInstanceInfo } from './utils/litefs.server.ts'
import { NonceProvider } from './utils/nonce-provider.ts'
import { makeTimings } from './utils/timing.server.ts'

// init()
// global.ENV = getEnv()

// if (ENV.MODE === 'production' && ENV.SENTRY_DSN) {
// 	import('./utils/monitoring.server.ts').then(({ init }) => init())
// }

type DocRequestArgs = Parameters<HandleDocumentRequestFunction>

export default async function handleRequest(...args: DocRequestArgs) {
	const [
		request,
		responseStatusCode,
		responseHeaders,
		remixContext,
		loadContext,
	] = args
	// const { currentInstance, primaryInstance } = await getInstanceInfo()
	// responseHeaders.set('fly-region', process.env.FLY_REGION ?? 'unknown')
	// responseHeaders.set('fly-app', process.env.FLY_APP_NAME ?? 'unknown')
	// responseHeaders.set('fly-primary-instance', primaryInstance)
	// responseHeaders.set('fly-instance', currentInstance)

	const nonce = String(loadContext.cspNonce) ?? undefined

	let didError = false;

		// NOTE: this timing will only include things that are rendered in the shell
		// and will not include suspended components and deferred loaders
		const timings = makeTimings('render', 'renderToReadableStream')

		const body = await renderToReadableStream(
			<NonceProvider value={nonce}>
				<RemixServer context={remixContext} url={request.url} />
			</NonceProvider>,
			{
				signal: request.signal,
				onError: (error: unknown) => {
					didError = true
					console.error(error)
				},
				nonce,
			},
		);

		if (isbot(request.headers.get("user-agent"))) {
			await body.allReady;
		}

		responseHeaders.set('Content-Type', 'text/html')
		responseHeaders.append('Server-Timing', timings.toString())

		return new Response(body, {
			headers: responseHeaders,
			status: didError ? 500 : responseStatusCode,
		});
}

export async function handleDataRequest(response: Response) {
	// const { currentInstance, primaryInstance } = await getInstanceInfo()
	// response.headers.set('fly-region', process.env.FLY_REGION ?? 'unknown')
	// response.headers.set('fly-app', process.env.FLY_APP_NAME ?? 'unknown')
	// response.headers.set('fly-primary-instance', primaryInstance)
	// response.headers.set('fly-instance', currentInstance)

	return response
}

export function handleError(
	error: unknown,
	{ request }: LoaderFunctionArgs | ActionFunctionArgs,
): void {
	// if (error instanceof Error) {
	// 	Sentry.captureRemixServerException(error, 'remix.server', request)
	// } else {
	// 	Sentry.captureException(error)
	// }
	console.error(error)
}
